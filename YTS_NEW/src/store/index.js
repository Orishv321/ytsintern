import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {},
  // getters: {
  //   getMovieList(state) {
  //     return state.movieList;
  //   },
  // },
  state: {
    test: "Hello from state"
  },
  actions: {},
  mutations: {}
}); // export default new Vuex.Store
