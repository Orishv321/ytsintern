import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import search from "./modules/serarch";

export default new Vuex.Store({
  modules: {
    search
  },
  // getters: {
  //   getMovieList(state) {
  //     return state.movieList;
  //   },
  // },
  state: {
    newTest: "NewTest",
    test: "Hello from state"
  },
  actions: {
    pullnewData({ commit }, newTestData) {
      commit('setNewTestValue', newTestData);
      console.log("Call from pullnewData");
    },
    checkDispatch() {
      // alert("from dispatch");
    },
    testAction({ commit, dispatch }, newVal) {
      console.log(commit);
      commit('setTestVal', newVal);
      dispatch('checkDispatch')
    }
  },
  mutations: {
    setNewTestValue(state, payload) {
      state.newTest = payload
    },

    setTestVal(state, payload) {
      state.test = payload
      console.log('>>>>', state)
    }
  }
}); // export default new Vuex.Store
