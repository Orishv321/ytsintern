import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "Home",
        component: () =>
            import(/* webpackChunkName: "about" */ "../Pages/Home.vue"),
    },
    {
        path: "/search",
        name: "Home",
        component: () =>
            import(/* webpackChunkName: "about" */ "../Pages/Search.vue"),
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;
